import 'package:flutter/material.dart';
import './Content.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

/*
membuat file baru bernama HomePage
 */
class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('nested scroll view'),
        ),
        body: DefaultTabController(
          length: 3,
          child: NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) => [
              const SliverToBoxAdapter(
                child: Content(),
              ),
              const SliverAppBar(
                pinned: true,
                elevation: 0,
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(0),
                  child: TabBar(tabs: [
                    Tab(
                      child: Text('Test'),
                    ),
                    Tab(
                      child: Text('Test'),
                    ),
                    Tab(
                      child: Text('Test'),
                    )
                  ]),
                ),
              )
            ],
            body: TabBarView(children: [
              Center(child: Text('data 1')),
              Center(child: Text('data 2')),
              Center(child: Text('data 3')),
            ]),
          ),
        ));
  }
}
