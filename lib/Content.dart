import 'package:flutter/material.dart';

/*
di file ini kita menyimpan content yabg nantinya akan ditampilkan
sebelum adanya widget nested scroll view,
atau bisa dibilang juga file ini akan ditampilkan diatas nested scroll view
*/
class Content extends StatelessWidget {
  const Content({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 150,
          color: Colors.amber,
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 150,
          color: Colors.amber,
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 150,
          color: Colors.amber,
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
